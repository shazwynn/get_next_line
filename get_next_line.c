/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 01:58:13 by algrele           #+#    #+#             */
/*   Updated: 2019/03/18 13:54:58 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/*
** find or create and return elemment of t_gnl list for given fd.
*/

static t_gnl	*get_list_elem(t_gnl **head, int fd)
{
	t_gnl *new;
	t_gnl *tmp;

	tmp = *head;
	while (tmp)
	{
		if (tmp->fd == fd)
			return (tmp);
		tmp = tmp->next;
	}
	if (!(new = ft_memalloc(sizeof(t_gnl))))
		return (NULL);
	new->fd = fd;
	new->nlines = 0;
	new->next = NULL;
	if (!(new->keep = NULL) && *head)
	{
		tmp = *head;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
		return (new);
	}
	*head = new;
	return (*head);
}

/*
** add current buffer to sum of all characters kept from previous reads
*/

static t_gnl	*join_buf(t_gnl *elem, char *buf)
{
	char	*tmp;

	tmp = NULL;
	if (buf && buf[0])
	{
		if (!elem->keep)
			elem->keep = ft_strdup(buf);
		else
		{
			tmp = ft_strjoin(elem->keep, buf);
			free(elem->keep);
			elem->keep = tmp;
		}
	}
	return (elem);
}

/*
** removes line if one of the macroed conditions in get_next_line.h are not met
** line size : MIN_LINE_SIZE and MAX_LINE_SIZE
** REMOVE_C_COMMENTS : line starts with "//" or norm appropriate comment format
** REMOVE_ME : remove lines containing specified string.
** reverse line : REV_LINE = 1
*/

static int		remove_if(char **line, int delete_me)
{
	const char	*tmp;

	tmp = (const char *)*line;
	if ((MIN_LINE_SIZE > 0 && ft_strlen(*line) < MIN_LINE_SIZE) ||
			(MAX_LINE_SIZE > 0 && ft_strlen(*line) > MAX_LINE_SIZE))
		delete_me = 1;
	if (delete_me == 0 && REMOVE_C_COMMENTS)
	{
		if (ft_strnequ(tmp, "//", 2) || ft_strnequ(tmp, "/*", 2) ||
				ft_strnequ(tmp, "**", 2) || ft_strnequ(tmp, "*/", 2))
			delete_me = 1;
	}
	if (delete_me == 0 && REMOVE_ME && ft_strstr(tmp, REMOVE_ME))
		delete_me = 1;
	if (delete_me)
	{
		ft_strdel(line);
		return (1);
	}
	if (REV_LINE)
		*line = ft_strrev(*line);
	return (0);
}

/*
** join buffer if there is a buffer.
** if read is over and there was no '\n' found, line is duplicate of elem->keep
** else find '\n', create line and cut elem->keep
** if line was set, return 1, set and deleted, return 2, else, return 0
*/

static int		find_line(t_gnl *elem, char *buf, char **line, int ret)
{
	size_t	i;

	i = 0;
	if ((elem = join_buf(elem, buf)) && elem->keep)
	{
		while (elem->keep[i] && !(ft_c_in_macro(elem->keep[i], SEPARATORS)))
			i++;
		if (i != ft_strlen(elem->keep) || ret == 0)
		{
			if (!(*line = ft_strsub(elem->keep, 0, i)))
				return (0);
			elem->nlines++;
			ret == 0 ? ft_strclr(elem->keep) : ft_memmove(elem->keep,
					elem->keep + i + 1, ft_strlen(elem->keep) - i);
			ft_strdel(&buf);
			if (remove_if(line, 0))
				return (2);
			return (1);
		}
	}
	return (0);
}

/*
** get_next_line reads fd and returns line read uptil and excluding '\n'
** or whatever characters are in charset SEPARATORS
** return 1 : line read
** return 0 : everything has been read or MAX_LINES have been read
** return -1 : error
*/

int				get_next_line(const int fd, char **line)
{
	static t_gnl	*list = NULL;
	t_gnl			*elem;
	char			*buf;
	int				ret;

	if (fd < 0 || !line || BUFF_SIZE <= 0 || !(buf = ft_strnew(BUFF_SIZE)))
		return (-1);
	if (!(elem = get_list_elem(&list, fd)))
		return (-1);
	if (MAX_LINES > 0 && elem->nlines == MAX_LINES)
		return (0);
	if ((ret = find_line(elem, buf, line, -1)))
		return (ret == 1 ? 1 : 2);
	while ((ret = read(fd, buf, BUFF_SIZE)))
	{
		if (ret == -1)
			return (-1);
		if ((ret = find_line(elem, buf, line, -1)))
			return (ret == 1 ? 1 : 2);
		ft_bzero(buf, BUFF_SIZE);
	}
	if (ret == 0 && elem->keep && elem->keep[0])
		return (find_line(elem, buf, line, ret));
	ft_strdel(&buf);
	return (0);
}
