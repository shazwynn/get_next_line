/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 11:09:41 by algrele           #+#    #+#             */
/*   Updated: 2018/05/30 21:16:49 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "libft.h"
# define BUFF_SIZE 32

# define SEPARATORS "\n"
# define MAX_LINES -1
# define MIN_LINE_SIZE -1
# define MAX_LINE_SIZE -1
# define REMOVE_C_COMMENTS 0
# define REMOVE_ME NULL
# define REV_LINE 0

typedef struct		s_gnl
{
	int				fd;
	int				nlines;
	char			*keep;
	struct s_gnl	*next;
}					t_gnl;

int					get_next_line(const int fd, char **line);

#endif
